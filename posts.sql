CREATE TABLE IF NOT EXISTS `posts` (
  `id` int NOT NULL AUTO_INCREMENT,
  `post_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `content` varchar(255) NOT NULL,
  `user_id` int NOT NULL,
  `topic_id` int NOT NULL,
  PRIMARY KEY (`id`),
	FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
	FOREIGN KEY (`topic_id`) REFERENCES `topics` (`id`)
	);

INSERT INTO `posts` (`post_date`, `content`, `user_id`, `topic_id`) VALUES
('2021-07-17','The band-composed album-closing suite on Kansas breakthrough album is testimony to Steinhardts tightly woven place in the ensemble. He has a few standout moments in the "Man Overboard" and "Release the Beavers" segments, but much of his contribution is understated and complementary to the overall orchestration',1,1),
('2021-07-19','Yet another US Olympian will be forced to withdraw from the Tokyo Olympics, as Seattle Storm forward Katie Lou Samuelson announced on her Instagram that she has tested positive for COVID-19.',1,5),
('2021-07-20','Zach LaVine has established himself as a star and one of the premier scorers in the entire NBA.  However, he’s entering the final year of his contract next season at the age of 26. ',2,5),
('2021-07-20',"C'est une édition exceptionnelle du Festival de Néoules qui aura lieu dans le Var cette année, puisqu'il se déroulera sur 4 soirs et ce dès demain !",2,3);