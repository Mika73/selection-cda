<?php
class Topic
{
	// DataManager
	private $dm;

	// Topic Properties
	public $id;
	public $title;
	public $category_id;
	public $user_id;
	public $label;
	public $email;

	// Constructor with Data manager
	public function __construct()
	{
		$conf = new Config;
		$this->dm = $conf->getDataManager();
	}

	// Read Topic
	public function get(int $id = null)
	{
		$topic = $this->dm->topic->get($id);
		return $topic;
	}
	// Create Topic
	public function create()
	{
		$topic = $this->dm->topic->create();
		return $topic;
	}
	// Update Topic
	public function update(int $id)
	{
		$topic = $this->dm->topic->update($id);
		return $topic;
	}

	// Delete Topic
	public function delete(int $id)
	{
		$topic = $this->dm->topic->delete($id);
		return $topic;
	}
}
