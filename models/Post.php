<?php
class Post
{
	// DataManager
	private $dm;

	// Post Properties
	public $id;
	public $post_date;
	public $content;
	public $user_id;
	public $topic_id;
	public $title;
	public $label;
	public $email;

	// Constructor with Data manager
	public function __construct()
	{
		$conf = new Config;
		$this->dm = $conf->getDataManager();
	}

	// Read Post
	public function get(int $id = null)
	{
		$post = $this->dm->post->get($id);
		return $post;
	}
	// Read Post by user id
	public function getByUserId(int $user_id)
	{
		$post = $this->dm->post->getByUserId($user_id);
		return $post;
	}
	// Create Post
	public function create()
	{
		$post = $this->dm->post->create();
		return $post;
	}
	// Update Post
	public function update(int $id)
	{
		$post = $this->dm->post->update($id);
		return $post;
	}

	// Delete Post
	public function delete(int $id)
	{
		$post = $this->dm->post->delete($id);
		return $post;
	}
}
