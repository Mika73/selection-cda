<?php
class User
{
	// DataManager
	private $dm;

	// User Properties
	public $id;
	public $email;
	public $password;
	public $birth_date;

	// Constructor with Data manager
	public function __construct()
	{
		$conf = new Config;
		$this->dm = $conf->getDataManager();
	}

	// Read User
	public function get(int $id = null)
	{
		$user = $this->dm->user->get($id);
		return $user;
	}
	// Create User
	public function create()
	{
		$user = $this->dm->user->create();
		return $user;
	}
	// Update User
	public function update(int $id)
	{
		$user = $this->dm->user->update($id);
		return $user;
	}

	// Delete User
	public function delete(int $id)
	{
		$user = $this->dm->user->delete($id);
		return $user;
	}

}
