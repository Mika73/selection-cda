<?php

class Category
{
	// DataManager
	private $dm;

	// Category Properties
	public $id;
	public $label;

	// Constructor with Data manager
	public function __construct()
	{
		$conf = new Config;
		$this->dm = $conf->getDataManager();
	}
	// Read Category
	public function get(int $id = null) {
		$category = $this->dm->category->get($id);
		return $category;
	}

	// Create Category
	public function create()
	{
		$category = $this->dm->category->create();
		return $category;
	}
	// Update Category
	public function update(int $id)
	{
		$category = $this->dm->category->update($id);
		return $category;
	}

	// Delete Category
	public function delete(int $id)
	{
		$category = $this->dm->category->delete($id);
		return $category;
	}
}
