# Projet sélection formation CDA

Simple PHP CRUD REST API programmes structured with DAO design pattern. Implimented from scratch with no framework.

## Quick Start

Import the sql files, change the params in the Config.php file to your own.

## URI Structure

REST APIs provide access to resources (data entities) via URI paths. To use a REST API, your application will make an HTTP request and parse the response. The REST API uses JSON as its communication format, and the standard HTTP methods like GET, PUT, POST and DELETE. URIs for REST API resource have the following structure:
```
http://host/selection_cda/api/api-name
```
## Resources

### User
**Get all**  
	[GET] http://host/selection_cda/api/user.php  
**Get by id**  
	[GET] http://host/selection_cda/api/user.php?id={id}  
**Create**  
	[POST] http://host/selection_cda/api/user.php  
	request body exemple  
	```  
	{  
    "email": "name@abc.com",  
    "password":"password",  
    "birth_date": "2021-07-22"  
	}  
	```  
**Update**  
	[PUT] http://host/selection_cda/api/user.php?id={id}  
**Delete**  
	[DELETE] http://host/selection_cda/api/user.php?id={id}  

### Post
**Get all**  
	[GET] http://host/selection_cda/api/post.php  
**Get by id**  
	[GET] http://host/selection_cda/api/post.php?id={id}  
**Get by user id**  
	[GET] http://host/selection_cda/api/post.php?user_id={user_id}  
**Create**  
	[POST] http://host/selection_cda/api/post.php  
	request body exemple  
	```  
	{  
    "content":"Write your grate content here...",  
    "user_id": "1",  
    "topic_id":"1"  
	}  
	```  
**Update**  
	[PUT] http://host/selection_cda/api/post.php?id={id}  
**Delete**  
	[DELETE] http://host/selection_cda/api/post.php?id={id}  
 	
### Category
**Get all**  
	[GET] http://host/selection_cda/api/category.php  
**Get by id**  
	[GET] http://host/selection_cda/api/category.php?id={id}  
**Create**  
	[POST] http://host/selection_cda/api/category.php  
	request body exemple  
	```      
	{  
    "label":"Category name"  
	}  
	```  
**Update**  
	[PUT] http://host/selection_cda/api/category.php?id={id}  
**Delete**  
	[DELETE] http://host/selection_cda/api/category.php?id={id}  

### Topic
**Get all**  
	[GET] http://host/selection_cda/api/topic.php  
**Get by id**  
	[GET] http://host/selection_cda/api/topic.php?id={id}  
**Create**  
	[POST] http://host/selection_cda/api/topic.php  
	request body exemple    
	```  
	{  
    "title": "Topic name",  
    "category_id":"1",  
    "user_id": "1"  
	}  
	```  
**Update**  
	[PUT] http://host/selection_cda/api/topic.php?id={id}  
**Delete**  
	[DELETE] http://host/selection_cda/api/topic.php?id={id}  

## Methodology

1. I studied about design patterns.
2. I studied about API REST.

## Encountered difficulties

1. I had a hard time designing the middle layer between application/business layer and the persistence layer.
2. I had trouble setting the path when reading external files.

## Resources

1. [DAO] 			https://www.youtube.com/watch?v=EVP35NXbQBU
2. [REST API] https://github.com/bradtraversy/php_rest_myblog  
							https://github.com/deatiger/basic-rest-api

## Author

Mika Ito

## Version

1.0.0