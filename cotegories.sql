CREATE TABLE IF NOT EXISTS `categories` (
  `id` int NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `categories` (`label`) VALUES
('Music'),
('Sports'),
('Foods'),
('Fashion'),
('Travel')
;