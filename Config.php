<?php
// For debug only
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include_once(__DIR__ . '/DataManager/DataManager.php');

class Config
{
	/**
	 * Name of the Data manager
	 * Idealy from config file
	 */
	const DATA_MANAGER = 'Sql';

	// DB Params
	private $host = 'localhost';
	private $db_name = 'selection_cda';
	private $username = 'mika';
	private $password = 'Mika-1234';
	private $conn;

	/**
	 * Instantiate a data manager according to resources
	 * @return dataManager object
	 */
	public function getDataManager()
	{
		$dataManagerName = 'DataManager' . self::DATA_MANAGER;
		include_once(__DIR__ . '/DataManager/' . $dataManagerName . '/' . $dataManagerName . '.php');
		$dm = new $dataManagerName;
		return $dm;
	}

	/**
	 * DB Connect
	 * @return $conn
	 */
	public function connect()
	{
		$this->conn = null;

		try {
			$this->conn = new PDO('mysql:host=' . $this->host . ';dbname=' . $this->db_name, $this->username, $this->password);
			$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch (PDOException $e) {
			echo 'Connection Error: ' . $e->getMessage();
		}

		return $this->conn;
	}

	/**
	 * Set Response Headers
	 * @param mixed $request
	 * 
	 * @return void
	 */
	public function setResponseHeaders($request)
	{
		header('Access-Control-Allow-Origin: *');
		header('Content-Type: application/json');
		switch ($request) {
			case 'GET':
				break;
			case 'POST':
				header('Access-Control-Allow-Methods: POST');
				header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');
				break;
			case 'PUT':
				header('Access-Control-Allow-Methods: PUT');
				header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');
				break;
			case 'DELETE':
				header('Access-Control-Allow-Methods: DELETE');
				header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');
			default:
				break;				
		}
	}
}
