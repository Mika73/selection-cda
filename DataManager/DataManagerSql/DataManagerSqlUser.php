<?php
class DataManagerSqlUser implements DataManager
{
	// DB stuff
	private $conn;
	private const DB_TABLE = 'users';

	// Constructor with DB
	public function __construct()
	{
		$conf = new Config();
		$this->conn = $conf->connect();
	}

	// Get user
	public function get(int $id = null): array
	{
		$users_arr = array();

		if (!is_null($id)) {
			$query = 'SELECT id, email, password, birth_date FROM ' . self::DB_TABLE . ' WHERE id=' . $id;
		} else {
			$query = 'SELECT id, email, password, birth_date FROM ' . self::DB_TABLE;
		}
		// Prepare statement
		$stmt = $this->conn->prepare($query);

		// Execute query
		$stmt->execute();
		// Get row count
		$num = $stmt->rowCount();

		// Check if any users
		if ($num > 0) {
			// User array
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				extract($row);

				$user_item = array(
					'id' => $id,
					'email' => $email,
					'password' => $password,
					'birth_date' => $birth_date,
				);

				// Push to "data"
				array_push($users_arr, $user_item);
			}
			return $users_arr;
		} else {
			return array('Error' => 'No User Found');
		}
	}

	// Create user
	public function create()
	{
		$data = json_decode(file_get_contents("php://input"));

		// Create query
		$query = 'INSERT INTO ' . self::DB_TABLE . ' SET email = :email, password = :password, birth_date = :birth_date';

		// Prepare statement
		$stmt = $this->conn->prepare($query);

		// Clean data
		$data->email = htmlspecialchars(strip_tags($data->email));
		$data->password = password_hash($data->password, PASSWORD_DEFAULT);
		$data->birth_date = filter_var(preg_replace("([^0-9/] | [^0-9-])", "", htmlentities($data->birth_date)));
		
		// Bind data
		$stmt->bindParam(':email', $data->email);
		$stmt->bindParam(':password', $data->password);
		$stmt->bindParam(':birth_date', $data->birth_date);

		// Execute query
		if ($stmt->execute()) {
			return array('message' => 'User Created');
		} else {
			// Print error if something goes wrong
			return printf("Error: %s.\n", $stmt->error);
		}
	}

	// Update user
	public function update($id)
	{
		$data = json_decode(file_get_contents("php://input"));

		// Create query
		$query = 'UPDATE ' . self::DB_TABLE .'
                                SET email = :email, password = :password, birth_date = :birth_date
                                WHERE id = :id';

		// Prepare statement
		$stmt = $this->conn->prepare($query);

		// Clean data
		$data->email = htmlspecialchars(strip_tags($data->email));
		$data->password = password_hash($data->password, PASSWORD_DEFAULT);
		$data->birth_date = filter_var(preg_replace("([^0-9/] | [^0-9-])", "", htmlentities($data->birth_date)));
		// Bind data
		$stmt->bindParam(':email', $data->email);
		$stmt->bindParam(':password',  $data->password);
		$stmt->bindParam(':birth_date', $data->birth_date);
		$stmt->bindParam(':id', $id);

		// Execute query
		if ($stmt->execute()) {
			return array('message' => 'User Updated');
		} else {
			// Print error if something goes wrong
			return printf("Error: %s.\n", $stmt->error);
		}
	}

	// Delete user
	public function delete($id)
	{
		// Create query
		$query = 'DELETE FROM ' . self::DB_TABLE . ' WHERE id = :id';

		// Prepare statement
		$stmt = $this->conn->prepare($query);

		// Bind data
		$stmt->bindParam(':id', $id);

		// Execute query
		if ($stmt->execute()) {
			return array('message' => 'User Deleted');
		} else {
			// Print error if something goes wrong
			return printf("Error: %s.\n", $stmt->error);
		}
	}
}
