<?php
class DataManagerSqlCategory implements DataManager
{
	// DB stuff
	private $conn;
	private const DB_TABLE = 'categories';

	// Constructor with DB
	public function __construct()
	{
		$conf = new Config();
		$this->conn = $conf->connect();
	}

	// Get category
	public function get(int $id = null): array
	{
		$categories_arr = array();

		if (!is_null($id)) {
			$query = 'SELECT id, label FROM ' . self::DB_TABLE . ' WHERE id=' . $id;
		} else {
			$query = 'SELECT id, label FROM ' . self::DB_TABLE;
		}
		// Prepare statement
		$stmt = $this->conn->prepare($query);

		// Execute query
		$stmt->execute();
		// Get row count
		$num = $stmt->rowCount();

		// Check if any categories
		if ($num > 0) {
			// Category array
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				extract($row);

				$category_item = array(
					'id' => $id,
					'label' => $label,
				);

				// Push to "data"
				array_push($categories_arr, $category_item);
			}
			return $categories_arr;
		} else {
			return array('Error' => 'No Category Found');
		}
	}

	// Create category
	public function create()
	{
		$data = json_decode(file_get_contents("php://input"));

		// Create query
		$query = 'INSERT INTO ' . self::DB_TABLE . ' SET label = :label';

		// Prepare statement
		$stmt = $this->conn->prepare($query);

		// Clean data
		$data->label = htmlspecialchars(strip_tags($data->label));

		// Bind data
		$stmt->bindParam(':label', $data->label);

		// Execute query
		if ($stmt->execute()) {
			return array('message' => 'Category Created');
		} else {
			// Print error if something goes wrong
			return printf("Error: %s.\n", $stmt->error);
		}
	}

	// Update category
	public function update($id)
	{
		$data = json_decode(file_get_contents("php://input"));

		// Create query
		$query = 'UPDATE ' . self::DB_TABLE. '
                                SET label = :label
                                WHERE id = :id';

		// Prepare statement
		$stmt = $this->conn->prepare($query);

		// Clean data
		$data->label = htmlspecialchars(strip_tags($data->label));

		// Bind data
		$stmt->bindParam(':label', $data->label);
		$stmt->bindParam(':id', $id);

		// Execute query
		if ($stmt->execute()) {
			return array('message' => 'Category Updated');
		} else {
			// Print error if something goes wrong
			return printf("Error: %s.\n", $stmt->error);
		}
	}

	// Delete category
	public function delete($id)
	{
		// Create query
		$query = 'DELETE FROM ' . self::DB_TABLE . ' WHERE id = :id';

		// Prepare statement
		$stmt = $this->conn->prepare($query);

		// Bind data
		$stmt->bindParam(':id', $id);

		// Execute query
		if ($stmt->execute()) {
			return array('message' => 'Category Deleted');
		}else{
			// Print error if something goes wrong
			return printf("Error: %s.\n", $stmt->error);
		}
	}
}
