<?php
include_once(__DIR__ . '/DataManagerSqlCategory.php');
include_once(__DIR__ . '/DataManagerSqlUser.php');
include_once(__DIR__ . '/DataManagerSqlTopic.php');
include_once(__DIR__ . '/DataManagerSqlPost.php');

class DataManagerSql
{
	public $user;
	public $category;
	public $topic;
	public $post;

	public function __construct()
	{
		$this->user = new DataManagerSqlUser();
		$this->category = new DataManagerSqlCategory();
		$this->topic = new DataManagerSqlTopic();
		$this->post = new DataManagerSqlPost();
	}
}
