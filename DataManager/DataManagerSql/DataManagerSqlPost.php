<?php
class DataManagerSqlPost implements DataManager
{
	// DB stuff
	private $conn;
	private const DB_TABLE = 'posts';

	// Constructor with DB
	public function __construct()
	{
		$conf = new Config();
		$this->conn = $conf->connect();
	}

	// Get post
	public function get(int $id = null): array
	{
		$posts_arr = array();

		if (!is_null($id)) {
			$query = 'SELECT p.id , p.post_date, p.content, p.user_id, p.topic_id, t.title, c.label, u.email
							FROM ' . self::DB_TABLE . ' p
							LEFT JOIN
								topics t ON p.topic_id = t.id
							LEFT JOIN
								categories c ON t.category_id = c.id
							LEFT JOIN
								users u ON p.user_id = u.id  		
							WHERE p.id=' . $id;
		} else {
			$query = 'SELECT p.id , p.post_date, p.content, p.user_id, p.topic_id, t.title, c.label, u.email
							FROM ' . self::DB_TABLE . ' p
							LEFT JOIN
								topics t ON p.topic_id = t.id
							LEFT JOIN
								categories c ON t.category_id = c.id
							LEFT JOIN
								users u ON p.user_id = u.id  		
							ORDER BY
								p.post_date DESC';
		}
		// Prepare statement
		$stmt = $this->conn->prepare($query);

		// Execute query
		$stmt->execute();
		// Get row count
		$num = $stmt->rowCount();

		// Check if any topics
		if ($num > 0) {
			// Topic array
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				extract($row);

				$post_item = array(
					'id' => $id,
					'post_date' => $post_date,
					'content' => $content,
					'user_id' => $user_id,
					'topic_id' => $topic_id,
					'title' => $title,
					'label' => $label,
					'email' => $email
				);

				// Push to "data"
				array_push($posts_arr, $post_item);
			}
			return $posts_arr;
		} else {
			return array('Error' => 'No Post Found');
		}
	}
	// Get post by user id
	public function getByUserId(int $user_id): array
	{
		$posts_arr = array();

		if (!is_null($user_id)) {
			$query = 'SELECT p.id , p.post_date, p.content, p.user_id, p.topic_id, t.title, c.label, u.email
							FROM ' . self::DB_TABLE . ' p
							LEFT JOIN
								topics t ON p.topic_id = t.id
							LEFT JOIN
								categories c ON t.category_id = c.id
							LEFT JOIN
								users u ON p.user_id = u.id  		
							WHERE p.user_id=' . $user_id;
		}
		// Prepare statement
		$stmt = $this->conn->prepare($query);

		// Execute query
		$stmt->execute();
		// Get row count
		$num = $stmt->rowCount();

		// Check if any topics
		if ($num > 0) {
			// Topic array
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)
			) {
				extract($row);

				$post_item = array(
						'id' => $id,
						'post_date' => $post_date,
						'content' => $content,
						'user_id' => $user_id,
						'topic_id' => $topic_id,
						'title' => $title,
						'label' => $label,
						'email' => $email
					);

				// Push to "data"
				array_push($posts_arr, $post_item);
			}
			return $posts_arr;
		} else {
			return array('Error' => 'No Post Found');
		}
	}

	// Create post
	public function create()
	{
		$data = json_decode(file_get_contents("php://input"));
		$today = date("Y-m-d");

		// Create query
		$query = 'INSERT INTO ' . self::DB_TABLE . ' SET post_date = :post_date, content = :content, user_id = :user_id, topic_id = :topic_id';

		// Prepare statement
		$stmt = $this->conn->prepare($query);

		// Clean data
		$data->content = htmlspecialchars(strip_tags($data->content));
		$data->user_id = htmlspecialchars(strip_tags($data->user_id));
		$data->topic_id = htmlspecialchars(strip_tags($data->topic_id));
		

		// Bind data
		$stmt->bindParam(':post_date', $today);
		$stmt->bindParam(':content', $data->content);
		$stmt->bindParam(':user_id', $data->user_id);
		$stmt->bindParam(':topic_id', $data->topic_id);

		// Execute query
		if ($stmt->execute()) {
			return array('message' => 'Post Created');
		} else {
			// Print error if something goes wrong
			return printf("Error: %s.\n", $stmt->error);
		}
	}

	// Update post
	public function update($id)
	{
		$data = json_decode(file_get_contents("php://input"));
		$today = date("Y-m-d");

		// Create query
		$query = 'UPDATE ' . self::DB_TABLE . ' 
						SET post_date = :post_date, 
								content = :content,
								user_id = :user_id, 
								topic_id = :topic_id
						WHERE id = :id';

		// Prepare statement
		$stmt = $this->conn->prepare($query);

		// Clean data
		$data->content = htmlspecialchars(strip_tags($data->content));
		$data->user_id = htmlspecialchars(strip_tags($data->user_id));
		$data->topic_id = htmlspecialchars(strip_tags($data->topic_id));

		// Bind data
		$stmt->bindParam(':post_date', $today);
		$stmt->bindParam(':content', $data->content);
		$stmt->bindParam(':user_id', $data->user_id);
		$stmt->bindParam(':topic_id', $data->topic_id);
		$stmt->bindParam(':id', $id);

		// Execute query
		if ($stmt->execute()) {
			return array('message' => 'Post Updated');
		} else {
			// Print error if something goes wrong
			return printf("Error: %s.\n", $stmt->error);
		}
	}

	// Delete post
	public function delete($id)
	{
		// Create query
		$query = 'DELETE FROM ' . self::DB_TABLE . ' WHERE id = :id';

		// Prepare statement
		$stmt = $this->conn->prepare($query);

		// Bind data
		$stmt->bindParam(':id', $id);

		// Execute query
		if ($stmt->execute()) {
			return array('message' => 'Post Deleted');
		} else {
			// Print error if something goes wrong
			return printf("Error: %s.\n", $stmt->error);
		}
	}
}
