<?php
class DataManagerSqlTopic implements DataManager
{
	// DB stuff
	private $conn;
	private const DB_TABLE = 'topics';

	// Constructor with DB
	public function __construct()
	{
		$conf = new Config();
		$this->conn = $conf->connect();
	}

	// Get topic
	public function get(int $id = null): array
	{
		$topics_arr = array();

		if (!is_null($id)) {
			$query = 'SELECT t.id, t.title, c.label, u.email 
							FROM ' . self::DB_TABLE . ' t
							LEFT JOIN
								categories c ON t.category_id = c.id
							LEFT JOIN
								users u ON t.user_id = u.id	  
							WHERE t.id=' . $id;
		} else {
			$query = 'SELECT t.id, t.title, c.label, u.email 
							FROM ' . self::DB_TABLE . ' t
							LEFT JOIN
								categories c ON t.category_id = c.id
							LEFT JOIN
								users u ON t.user_id = u.id';	  
		}
		// Prepare statement
		$stmt = $this->conn->prepare($query);

		// Execute query
		$stmt->execute();
		// Get row count
		$num = $stmt->rowCount();

		// Check if any topics
		if ($num > 0) {
			// Topic array
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				extract($row);

				$topic_item = array(
					'id' => $id,
					'title' => $title,
					'label' => $label,
					'email' => $email,
				);

				// Push to "data"
				array_push($topics_arr, $topic_item);
			}
			return $topics_arr;
		} else {
			return array('Error' => 'No Topic Found');
		}
	}

	// Create topic
	public function create()
	{
		$data = json_decode(file_get_contents("php://input"));

		// Create query
		$query = 'INSERT INTO ' . self::DB_TABLE . ' SET title = :title, category_id = :category_id, user_id = :user_id';

		// Prepare statement
		$stmt = $this->conn->prepare($query);

		// Clean data
		$data->title = htmlspecialchars(strip_tags($data->title));
		$data->category_id = htmlspecialchars(strip_tags($data->category_id));
		$data->user_id = htmlspecialchars(strip_tags($data->user_id));

		// Bind data
		$stmt->bindParam(':title', $data->title);
		$stmt->bindParam(':category_id', $data->category_id);
		$stmt->bindParam(':user_id', $data->user_id);

		// Execute query
		if ($stmt->execute()) {
			return array('message' => 'Topic Created');
		} else {
			// Print error if something goes wrong
			return printf("Error: %s.\n", $stmt->error);
		}
	}

	//Update topic
	public function update($id)
	{
		$data = json_decode(file_get_contents("php://input"));

		// Create query
		$query = 'UPDATE ' . self::DB_TABLE . '
                    SET title = :title, 
											category_id = :category_id, 
											user_id = :user_id
                    WHERE id = :id';

		// Prepare statement
		$stmt = $this->conn->prepare($query);

		// Clean data
		$data->title = htmlspecialchars(strip_tags($data->title));
		$data->category_id = htmlspecialchars(strip_tags($data->category_id));
		$data->user_id = htmlspecialchars(strip_tags($data->user_id));

		// Bind data
		$stmt->bindParam(':title', $data->title);
		$stmt->bindParam(':category_id', $data->category_id);
		$stmt->bindParam(':user_id', $data->user_id);
		$stmt->bindParam(':id', $id);

		// Execute query
		if ($stmt->execute()) {
			return array('message' => 'Topic Updated');
		} else {
			// Print error if something goes wrong
			return printf("Error: %s.\n", $stmt->error);
		}
	}

	// Delete topic
	public function delete($id)
	{
		// Create query
		$query = 'DELETE FROM ' . self::DB_TABLE . ' WHERE id = :id';

		// Prepare statement
		$stmt = $this->conn->prepare($query);

		// Bind data
		$stmt->bindParam(':id', $id);

		// Execute query
		if ($stmt->execute()) {
			return array('message' => 'Topic Deleted');
		} else {
			// Print error if something goes wrong
			return printf("Error: %s.\n", $stmt->error);
		}
	}
}
