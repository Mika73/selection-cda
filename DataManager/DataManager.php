<?php
interface DataManager {
  public function get(int $id = null);
  public function create();
  public function update($id);
  public function delete($id);
} 
?>