CREATE TABLE IF NOT EXISTS `topics` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `category_id` int NOT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`),
	FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
	FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
	);

INSERT INTO `topics` (`title`, `category_id`, `user_id`) VALUES
('Roc',1,1),
('Hip Hop',1,1),
('Reggae',1,1),
('Foot',2,1),
('Basketball',2,1),
('Baseball',1,1);