CREATE TABLE IF NOT EXISTS `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
	`password` varchar(255) NOT NULL,
  `birth_date` datetime,
  PRIMARY KEY (`id`)
);

INSERT INTO `users` (`email`,`password`,`birth_date`) VALUES
('mika@abc.com','password1','1976-10-17'),
('ilir@abc.com','password2','1963-07-22');