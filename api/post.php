<?php
include_once('../Config.php');
include_once('../models/Post.php');

// Data preparation
$conf = new Config();

// Instantiate category object
$post = new Post();

// Set Response Headers
$conf->setResponseHeaders($_SERVER['REQUEST_METHOD']);

// Judgment of processing by header request
switch ($_SERVER['REQUEST_METHOD']) {

	case 'GET':
		// Read
		if (isset($_GET['user_id'])) {
			// Read post by user id
			$post = $post->getByUserId(intval($_GET['user_id']));
		} else {
			if (isset($_GET['id'])) {
				// Read post by id
				$post = $post->get(intval($_GET['id']));
			} else {
				// Read all posts
				$post = $post->get(null);
			}
		}
		echo json_encode($post);
		break;

	case 'POST':
		// Create
		$post = $post->create();
		echo json_encode($post);
		break;

	case 'PUT':
		// Update
		// If ID is not specified or if the specified ID does not exist, error
		if (isset($_GET['id'])) {
			$idIsExist = $post->get(intval($_GET['id']));
			if (!array_key_exists('Error', $idIsExist)) {
				$post = $post->update(intval($_GET['id']));
				echo json_encode($post);
			} else {
				echo json_encode(array('Error' => 'ID not found'));
			}
		} else {
			echo json_encode(array('Error' => 'No ID specified in the request'));
		}
		break;
	case 'DELETE':
		//Delete
		// If ID is not specified or if the specified ID does not exist, error
		if (isset($_GET['id'])) {
			$idIsExist = $post->get(intval($_GET['id']));
			if (!array_key_exists('Error', $idIsExist)) {
				$post = $post->delete(intval($_GET['id']));
				echo json_encode($post);
			} else {
				echo json_encode(array('Error' => 'ID not found'));
			}
		} else {
			echo json_encode(array('Error' => 'No ID specified in the request'));
		}
		break;
		// Error when request is other than Get Post Put Delete
	default:
		echo json_encode(array('Error' => 'Error in the request parameters'));
}
