<?php
include_once('../Config.php');
include_once('../models/Category.php');

// Data preparation
$conf = new Config();

// Instantiate category object
$category = new Category();

// Set Response Headers
$conf->setResponseHeaders($_SERVER['REQUEST_METHOD']);

// Judgment of processing by header request
switch ($_SERVER['REQUEST_METHOD']) {

	case 'GET':
		// Read
		if (isset($_GET['id'])) {
			// Read category by id
			$category = $category->get(intval($_GET['id']));
		} else {
			// Read all categories
			$category = $category->get(null);
		}
		echo json_encode($category);
		break;

	case 'POST':
		// Create
		$category = $category->create();
		echo json_encode($category);
		break;

	case 'PUT':
		// Update
		// If ID is not specified or if the specified ID does not exist, error
		if (isset($_GET['id'])) {
			$idIsExist = $category->get(intval($_GET['id']));
			if (!array_key_exists('Error', $idIsExist)) {
				$category = $category->update(intval($_GET['id']));
				echo json_encode($category);
			} else {
				echo json_encode(array('Error' => 'ID not found'));
			}
		} else {
			echo json_encode(array('Error' => 'No ID specified in the request'));
		}
		break;
	case 'DELETE':
		//Delete
		// If ID is not specified or if the specified ID does not exist, error
		if (isset($_GET['id'])) {
			$idIsExist = $category->get(intval($_GET['id']));
			if (!array_key_exists('Error', $idIsExist)) {
				$category = $category->delete(intval($_GET['id']));
				echo json_encode($category);
			} else {
				echo json_encode(array('Error' => 'ID not found'));
			}
		} else {
			echo json_encode(array('Error' => 'No ID specified in the request'));
		}
		break;

	default:
}
