<?php
include_once('../Config.php');
include_once('../models/Topic.php');

// Data preparation
$conf = new Config();

// Instantiate category object
$topic = new Topic();

// Set Response Headers
$conf->setResponseHeaders($_SERVER['REQUEST_METHOD']);

// Judgment of processing by header request
switch ($_SERVER['REQUEST_METHOD']) {

	case 'GET':
		// Read
		if (isset($_GET['id'])) {
			// Read topic by id
			$topic = $topic->get(intval($_GET['id']));
		} else {
			// Read all topics
			$topic = $topic->get(null);
		}
		echo json_encode($topic);
		break;

	case 'POST':
		// Create
		$topic = $topic->create();
		echo json_encode($topic);
		break;

	case 'PUT':
		// Update
		// If ID is not specified or if the specified ID does not exist, error
		if (isset($_GET['id'])) {
			$idIsExist = $topic->get(intval($_GET['id']));
			if (!array_key_exists('Error', $idIsExist)) {
				$topic = $topic->update(intval($_GET['id']));
				echo json_encode($topic);
			} else {
				echo json_encode(array('Error' => 'ID not found'));
			}
		} else {
			echo json_encode(array('Error' => 'No ID specified in the request'));
		}
		break;
	case 'DELETE':
		//Delete
		// If ID is not specified or if the specified ID does not exist, error
		if (isset($_GET['id'])) {
			$idIsExist = $topic->get(intval($_GET['id']));
			if (!array_key_exists('Error', $idIsExist)) {
				$topic = $topic->delete(intval($_GET['id']));
				echo json_encode($topic);
			} else {
				echo json_encode(array('Error' => 'ID not found'));
			}
		} else {
			echo json_encode(array('Error' => 'No ID specified in the request'));
		}
		break;

	default:
}
