<?php
include_once('../Config.php');
include_once('../models/User.php');

// Data preparation
$conf = new Config();

// Instantiate category object
$user = new User();

// Set Response Headers
$conf->setResponseHeaders($_SERVER['REQUEST_METHOD']);

// Judgment of processing by header request
switch ($_SERVER['REQUEST_METHOD']) {

	case 'GET':
		// Read
		if (isset($_GET['id'])) {
			// Read user by id
			$user = $user->get(intval($_GET['id']));
		} else {
			// Read all users
			$user = $user->get(null);
		}
		echo json_encode($user);
		break;

	case 'POST':
		// Create
		$user = $user->create();
		echo json_encode($user);
		break;

	case 'PUT':
		// Update
		// If ID is not specified or if the specified ID does not exist, error
		if (isset($_GET['id'])) {
			$idIsExist = $user->get(intval($_GET['id']));
			if (!array_key_exists('Error', $idIsExist)) {
				$user = $user->update(intval($_GET['id']));
				echo json_encode($user);
			} else {
				echo json_encode(array('Error' => 'ID not found'));
			}
		} else {
			echo json_encode(array('Error' => 'No ID specified in the request'));
		}
		break;
	case 'DELETE':
		//Delete
		// If ID is not specified or if the specified ID does not exist, error
		if (isset($_GET['id'])) {
			$idIsExist = $user->get(intval($_GET['id']));
			if (!array_key_exists('Error', $idIsExist)) {
				$user = $user->delete(intval($_GET['id']));
				echo json_encode($user);
			} else {
				echo json_encode(array('Error' => 'ID not found'));
			}
		} else {
			echo json_encode(array('Error' => 'No ID specified in the request'));
		}
		break;

	default:
}
